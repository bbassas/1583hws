import java.util.Date;

public class SchoolEvent extends GeneralEvent

{
    public String courseName;
    
    public	SchoolEvent(String name,String	location, Date date,String course)
    {
    super(name,location,date);
    this.courseName = course;
    }
    
    public String getCourseName()
    {
        return this.courseName;
    }
    
    public String toString()
    {
        return super.eventName + ", " + super.eventLocation + ", " + super.eventDate.toString() + ", " + this.courseName;
    }
}