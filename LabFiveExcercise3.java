import java.util.Scanner;

public class LabFiveExcercise3

{
     
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        
        int [] array1 = new int [3];
        int [] array2 = new int [3];
        
          System.out.println("Enter 6 integers to compare:");
        {
            for (int i = 0; i<array1.length; i++)
            {
                array1 [i] = input.nextInt();
            }
            
             for (int i = 0; i<array2.length;i++)
             {
                array2 [i] = input.nextInt();
             }
             System.out.println("Your answer is: "+dotProduct(array1,array2));
        }
        
    }
       public static int dotProduct(int[] array1,int[] array2)
        {
            int sum = 0;
            int product;
            for (int i = 0; i < array1.length; i++)
            {
            product = array1[i] * array2[i];
            sum += product;
            }
            return sum;
        }
    
}