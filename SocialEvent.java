import java.util.Date;
public class SocialEvent extends GeneralEvent

{
    boolean byob;
    
    public SocialEvent(String name,String location, Date date, boolean byob)
    {
        super(name, location, date);
        this.byob = byob;
    }
    public void getByob()
    {
        this.byob = byob;
    }
    
    public String toString()
    {
        return super.eventName + ", " + super.eventLocation + ", " +super.eventDate.toString() + ", " +this.byob;
    }
}