public class Fraction
{
    private int numerator;
    private int denominator;
    
    
    public Fraction(int numerator, int denominator)
    {
        this.numerator = numerator;
        this.denominator = denominator;
        System.out.printf("%s\n",this);

    }
    public int getDemoninator()
    {
        return this.denominator;
    }
    public int getNumerator()
    {
        return this.numerator;
    }
    public Fraction add(Fraction otherFraction)
    {
      int commonDenominator = this.getCommonDenom(this, otherFraction);
      int numerator = this.getNumerator() * otherFraction.getDemoninator() + this.getDemoninator()*otherFraction.getNumerator();
      Fraction sum = new Fraction(numerator, commonDenominator);
      return sum;
    }
    public Fraction subtract(Fraction otherFraction)
    {
      int commonDenominator = this.getCommonDenom(this, otherFraction);
      int numerator = this.getNumerator() * otherFraction.getDemoninator() - this.getDemoninator()*otherFraction.getNumerator();
      Fraction sum = new Fraction(numerator, commonDenominator);
      return sum;
    }
    public Fraction multiply(Fraction otherFraction)
    {
      int numerator = this.getNumerator() * otherFraction.getNumerator();
      int denominator = this.getDemoninator() * otherFraction.getDemoninator();
      Fraction sum = new Fraction(numerator, denominator);
      return sum;
    }
     public Fraction divide(Fraction otherFraction)
    {
      int numerator = this.getNumerator() * otherFraction.getDemoninator();
      int denominator = this.getDemoninator() * otherFraction.getNumerator();
      Fraction sum = new Fraction(numerator, denominator);
      return sum;
    }
    public int getCommonDenom(Fraction fraction1, Fraction fraction2)
    {
        int commonDenon = fraction1.getDemoninator() *fraction2.getDemoninator();
        return commonDenon;
    }
    public String toString()
    {
        return String.format("%d/%d",numerator,denominator);
    }
    
}

