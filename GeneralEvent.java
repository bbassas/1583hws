import java.util.Date;
public class GeneralEvent implements Event

{
    protected String eventName;
    protected String eventLocation;
    protected Date eventDate;

    public GeneralEvent(String	name,String	location, Date date)
    {
        this.eventName = name;
        this.eventLocation	= location;
        this.eventDate = date;
    }

    public	String	getName()
    {
    return	this.eventName;
    }
    
    public	String	getLocation()
    {
    return	this.eventLocation;
    }
    
    public	String	getDate()
    {
    return	this.eventDate.toString();
    }
    
    public String toString()
    {
        return eventName + ", " + eventDate + ", " + eventLocation;
    }
}
