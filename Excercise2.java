import java.util.Scanner; //imports scanner

public class Excercise2 

{ 
    public static void main(String[] args)
    
    {
        Scanner input = new Scanner(System.in); //creates scanner object
        int age; //variable for age
        
        System.out.println("Please enter your age: ");
        age = input.nextInt();
        
        if (age <=20)
        
        {
            System.out.println("Sorry kid, no cocktails for you.");
        }
        else
        {
            System.out.println("Pick your poison, its 5 o’clock somewhere!");
        }
        
    }
    
}