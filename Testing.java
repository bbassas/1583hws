import java.util.Date;

public class Testing

{
    
    public static void main(String[] args)
    {
        Date date = new Date();
        
         Event [] events = new Event [2];
         
         SchoolEvent party1 = new SchoolEvent("Party_1","Math 212", date,"CSCI - 1581");
         SocialEvent party2 = new SocialEvent("Party_2", "Math 212", date, true);
         
         SocialEvent gather1 = new SocialEvent("Gather_1", "Superdome", date, false);
         SchoolEvent gather2 = new SchoolEvent("Gather_2", "Superdome", date, "CSCI - 1581");
         
         events[0] = party1;
         events[1] = party2;
         
          for (Event event: events)
            {
            System.out.println(event.toString() );
            }
    }
}